﻿using Ciber.Infrastructure.Infastructures;
using Ciber.Infrastructure.IRepositories;
using Ciber.Models.Entities;

namespace Ciber.Infrastructure.Repositories
{
    public class CategoryRepository : GenericRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
