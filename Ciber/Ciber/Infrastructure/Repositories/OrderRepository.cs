﻿using Ciber.Infrastructure.Infastructures;
using Ciber.Infrastructure.IRepositories;
using Ciber.Models.Entities;

namespace Ciber.Infrastructure.Repositories
{
    public class OrderRepository:GenericRepository<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationDbContext context): base(context)
        {

        }
    }
}
