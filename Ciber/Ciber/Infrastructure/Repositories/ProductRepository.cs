﻿using Ciber.Infrastructure.Infastructures;
using Ciber.Infrastructure.IRepositories;
using Ciber.Models.Entities;

namespace Ciber.Infrastructure.Repositories
{
    public class ProductRepository:GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(ApplicationDbContext context):base(context)
        {

        }
    }
}
