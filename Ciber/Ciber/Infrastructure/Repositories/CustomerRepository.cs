﻿using Ciber.Infrastructure.Infastructures;
using Ciber.Infrastructure.IRepositories;
using Ciber.Models.Entities;

namespace Ciber.Infrastructure.Repositories
{
    public class CustomerRepository:GenericRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(ApplicationDbContext context):base(context)
        {

        }
    }
}
