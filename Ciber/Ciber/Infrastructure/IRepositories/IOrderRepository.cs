﻿using Ciber.Infrastructure.Infastructures;
using Ciber.Models.Entities;

namespace Ciber.Infrastructure.IRepositories
{
    public interface IOrderRepository : IGenericRepository<Order>
    {
    }
}
