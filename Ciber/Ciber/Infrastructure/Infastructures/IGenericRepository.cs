﻿using Ciber.Models.BaseEntity;
using Ciber.ViewModels.BaseEntityVms;
using Microsoft.EntityFrameworkCore.Query;
using System.Linq.Expressions;

namespace Ciber.Infrastructure.Infastructures
{
    public interface IGenericRepository<TEntity> where TEntity : class, IEntityBase
    {
        Task<IList<TEntity>> GetAll(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? includes = null);

        Task<PagedVm<TEntity>> GetAllWithPaging(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>,
            IOrderedQueryable<TEntity>> orderBy = null,
            bool isDeleted = false, string keyword = null, int pageIndex = 1, int pageSize = 10, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? includes = null);

        Task<TEntity> GetById(params object[] keyValues);

        Task Add(TEntity entity);

        void Update(TEntity entity);

        void Delete(params object[] keyValues);

        Task<IList<TEntity>> Find(Expression<Func<TEntity, bool>> condition, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? includes = null);
    }
}