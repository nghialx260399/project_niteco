﻿
using Ciber.Infrastructure.IRepositories;

namespace Ciber.Infrastructure.Infastructures
{
    public interface IUnitOfWork : IDisposable
    {
        ICategoryRepository CategoryRepository { get; }

        IProductRepository ProductRepository { get; }

        ICustomerRepository CustomerRepository { get; }

        IOrderRepository OrderRepository { get; }

        ApplicationDbContext ApplicationDbContext { get; }

        Task<int> SaveChanges();
    }
}