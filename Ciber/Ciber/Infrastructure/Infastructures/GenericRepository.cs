﻿using Ciber.Models.BaseEntity;
using Ciber.ViewModels.BaseEntityVms;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System.Linq.Expressions;

namespace Ciber.Infrastructure.Infastructures
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class, IEntityBase
    {
        protected readonly ApplicationDbContext Context;

        protected DbSet<TEntity> DbSet;

        public GenericRepository(ApplicationDbContext context)
        {
            Context = context;
            DbSet = Context.Set<TEntity>();
        }

        public async Task Add(TEntity entity)
        {
            await DbSet.AddAsync(entity);
        }

        public void Delete(params object[] keyValues)
        {
            var entityExisting = this.DbSet.Find(keyValues);
            if (entityExisting != null)
            {
                this.DbSet.Remove(entityExisting);
                return;
            }
            throw new ArgumentNullException($"{string.Join(";", keyValues)} was not found in the {typeof(TEntity)}");
        }

        public async Task<IList<TEntity>> Find(Expression<Func<TEntity, bool>> condition, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? includes = null)
        {
            IQueryable<TEntity> query = DbSet;
            if(includes != null)
                query = includes(query);
            return await query.Where(condition).ToListAsync();
        }

        public async Task<IList<TEntity>> GetAll(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? includes = null)
        {
            IQueryable<TEntity> query = DbSet;

            if (includes != null)
            {
                query = includes(query);
            }

            if (orderBy != null)
                query = orderBy(query);

            return await query.ToListAsync();
        }

        public async Task<PagedVm<TEntity>> GetAllWithPaging(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            bool isDeleted = false, string keyword = null, int pageIndex = 1, int pageSize = 10, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? includes = null)
        {
            IQueryable<TEntity> query = this.DbSet;
            if (filter != null)
            {
               query = query.Where(filter);
            }
            if (!isDeleted)
            {
                query = query.Where(x => !x.IsDeleted);
            }

            int totalRows = query.Count();
            int totalPage = (int)Math.Ceiling((decimal)totalRows / pageSize);

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            if (includes != null)
            {
                query = includes(query);
            }

            query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);


            return new PagedVm<TEntity>(query, pageIndex, pageSize, totalPage);
        }

        public async Task<TEntity> GetById(params object[] keyValues)
        {
            return await DbSet.FindAsync(keyValues);
        }

        public void Update(TEntity entity)
        {
            DbSet.Update(entity);
        }
    }
}