﻿
using Ciber.Infrastructure.IRepositories;
using Ciber.Infrastructure.Repositories;

namespace Ciber.Infrastructure.Infastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext context;
        private ICategoryRepository categoryRepository;
        private IProductRepository productRepository;
        private ICustomerRepository customerRepository;
        private IOrderRepository orderRepository;

        public UnitOfWork(ApplicationDbContext context)
        {
            this.context = context;
        }

        public ApplicationDbContext ApplicationDbContext => this.context;

        public ICategoryRepository CategoryRepository => this.categoryRepository ??= new CategoryRepository(this.context);

        public IProductRepository ProductRepository => this.productRepository ??= new ProductRepository(this.context);

        public IOrderRepository OrderRepository => this.orderRepository ??= new OrderRepository(this.context);

        public ICustomerRepository CustomerRepository => this.customerRepository ??= new CustomerRepository(this.context);

        public void Dispose()
        {
            this.context.Dispose();
        }

        public async Task<int> SaveChanges()
        {
            return await this.context.SaveChangesAsync();
        }
    }
}