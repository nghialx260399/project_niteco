﻿using Ciber.Models.Entities;

namespace Ciber.Services.Categories
{
    public interface ICategoryService
    {
        Task<IList<Category>> GetAll();

        Task<bool> Create(Category category);

        Task<Category> GetById(int id);

        Task<bool> Update(Category category);

        Task<bool> Delete(int id);
    }
}