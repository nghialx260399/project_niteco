﻿using AutoMapper;
using Ciber.Infrastructure.Infastructures;
using Ciber.Models.Entities;

namespace Ciber.Services.Categories
{
    public class CategoryService: ICategoryService
    {
        private readonly IUnitOfWork unitOfWork;

        public CategoryService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Create(Category category)
        {
            try
            {
                await this.unitOfWork.CategoryRepository.Add(category);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                unitOfWork.CategoryRepository.Delete(id);
                await unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<IList<Category>> GetAll()
        {
            var categories = await this.unitOfWork.CategoryRepository.GetAll();
            return categories;
        }

        public async Task<Category> GetById(int id)
        {
            var category = await unitOfWork.CategoryRepository.GetById(id);
            return category;
        }

        public async Task<bool> Update(Category category)
        {
            try
            {
                this.unitOfWork.CategoryRepository.Update(category);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
