﻿using Ciber.Infrastructure.Infastructures;
using Ciber.Models.Entities;

namespace Ciber.Services.Customers
{
    public class CustomerService: ICustomerService
    {
        private readonly IUnitOfWork unitOfWork;

        public CustomerService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Create(Customer customer)
        {
            try
            {
                await this.unitOfWork.CustomerRepository.Add(customer);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                unitOfWork.CustomerRepository.Delete(id);
                await unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<IList<Customer>> GetAll()
        {
            var customers = await this.unitOfWork.CustomerRepository.GetAll();
            return customers;
        }

        public async Task<Customer> GetById(int id)
        {
            var category = await unitOfWork.CustomerRepository.GetById(id);
            return category;
        }

        public async Task<bool> Update(Customer customer)
        {
            try
            {
                this.unitOfWork.CustomerRepository.Update(customer);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
