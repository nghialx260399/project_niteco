﻿using Ciber.Models.Entities;

namespace Ciber.Services.Customers
{
    public interface ICustomerService
    {
        Task<IList<Customer>> GetAll();

        Task<bool> Create(Customer customer);

        Task<Customer> GetById(int id);

        Task<bool> Update(Customer customer);

        Task<bool> Delete(int id);
    }
}