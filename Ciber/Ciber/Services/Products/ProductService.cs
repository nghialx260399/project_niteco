﻿using Ciber.Infrastructure.Infastructures;
using Ciber.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace Ciber.Services.Products
{
    public class ProductService: IProductService
    {
        private readonly IUnitOfWork unitOfWork;

        public ProductService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Create(Product product)
        {
            try
            {
                await this.unitOfWork.ProductRepository.Add(product);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                unitOfWork.ProductRepository.Delete(id);
                await unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<IList<Product>> GetAll()
        {
            Func<IQueryable<Product>, IIncludableQueryable<Product, object>> includes = x => x.Include(x => x.Category);
            var products = await this.unitOfWork.ProductRepository.GetAll(includes: includes);
            return products;
        }

        public async Task<Product> GetById(int id)
        {
            var product = await unitOfWork.ProductRepository.GetById(id);
            return product;
        }

        public async Task<bool> Update(Product product)
        {
            try
            {
                this.unitOfWork.ProductRepository.Update(product);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
