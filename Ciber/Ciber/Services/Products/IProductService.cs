﻿using Ciber.Models.Entities;

namespace Ciber.Services.Products
{
    public interface IProductService
    {
        Task<IList<Product>> GetAll();

        Task<bool> Create(Product product);

        Task<Product> GetById(int id);

        Task<bool> Update(Product product);

        Task<bool> Delete(int id);
    }
}