﻿using Ciber.Infrastructure.Infastructures;
using Ciber.Models.Entities;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore;
using Ciber.ViewModels.BaseEntityVms;
using System.Linq.Expressions;

namespace Ciber.Services.Orders
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ILogger<OrderService> _logger;
        public OrderService(IUnitOfWork unitOfWork, ILogger<OrderService> logger)
        {
            this.unitOfWork = unitOfWork;
            this._logger = logger;
        }

        public async Task<bool> Create(Order order)
        {
            try
            {
                var product = await this.unitOfWork.ProductRepository.GetById(order.ProductId);
                if(product != null && product.Quantity >= order.Amount)
                {
                    await this.unitOfWork.OrderRepository.Add(order);
                    await this.unitOfWork.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Create Order Failed - {ex.Message} - {ex.StackTrace}");
                return false;
            }
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                unitOfWork.OrderRepository.Delete(id);
                await unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<PagedVm<Order>> GetAll(string keyword, Expression<Func<Order, bool>> filter, Func<IQueryable<Order>, IOrderedQueryable<Order>> orderBy, int pageSize, int pageIndex)
        {
            Func<IQueryable<Order>, IIncludableQueryable<Order, object>> includes = x => x.Include(x => x.Customer).Include(x=>x.Product).Include(x=>x.Product.Category);
            var orders = await this.unitOfWork.OrderRepository.GetAllWithPaging(pageIndex:pageIndex, pageSize:pageSize, includes:includes, filter:filter, orderBy:orderBy);
            return orders;
        }

        public async Task<Order> GetById(int id)
        {
            var order = await unitOfWork.OrderRepository.GetById(id);
            return order;
        }

        public async Task<bool> Update(Order order)
        {
            try
            {
                this.unitOfWork.OrderRepository.Update(order);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
