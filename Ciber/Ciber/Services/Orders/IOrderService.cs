﻿using Ciber.Models.Entities;
using Ciber.ViewModels.BaseEntityVms;
using System.Linq.Expressions;

namespace Ciber.Services.Orders
{
    public interface IOrderService
    {
        Task<PagedVm<Order>> GetAll(string keyword, Expression<Func<Order, bool>> filter, Func<IQueryable<Order>, IOrderedQueryable<Order>> orderBy, int pageSize, int pageIndex);

        Task<bool> Create(Order order);

        Task<Order> GetById(int id);

        Task<bool> Update(Order order);

        Task<bool> Delete(int id);
    }
}