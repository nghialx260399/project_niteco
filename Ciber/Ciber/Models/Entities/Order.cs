﻿using Ciber.Models.BaseEntity;

namespace Ciber.Models.Entities
{
    public class Order:EntityBase
    {
        public int CustomerId { get; set; }

        public Customer? Customer { get; set; }

        public int ProductId { get; set; }

        public Product? Product { get; set; }

        public int Amount { get; set; }

        public DateTime OrderDate { get; set; }
    }
}
