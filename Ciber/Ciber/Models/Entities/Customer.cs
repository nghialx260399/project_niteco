﻿using Ciber.Models.BaseEntity;

namespace Ciber.Models.Entities
{
    public class Customer:EntityBase
    {
        public string Name { get; set; }

        public string Address { get; set; }

        public IEnumerable<Order>? Orders { get; set; }
    }
}
