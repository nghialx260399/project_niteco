﻿namespace Ciber.ViewModels.BaseEntityVms
{
    public interface IEntityBaseVm
    {
        bool IsDeleted { get; set; }

        DateTime CreatedOn { get; set; }

        DateTime UpdatedOn { get; set; }
    }
}