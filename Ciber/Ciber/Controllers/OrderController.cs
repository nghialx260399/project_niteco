﻿using Ciber.Models.Entities;
using Ciber.Services.Orders;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Hosting;
using System.Linq.Expressions;

namespace Ciber.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        private readonly IOrderService orderService;

        public OrderController(IOrderService orderService)
        {
            this.orderService = orderService;
        }
        public async Task<IActionResult> Index(string keyword = null,string sortOrder = null, int pageIndex = 1, int pageSize = 5)
        {
            Expression<Func<Order, bool>> filter = null;
            if (!string.IsNullOrEmpty(keyword))
            {
                ViewBag.Keyword = keyword;
                keyword = keyword.ToLower();
                filter = x => x.Product.Name.ToLower().Contains(keyword) || x.Product.Category.Name.ToLower().Contains(keyword);
            }

            ViewBag.ProductSort = sortOrder == "product_desc" ? "product_asc" : "product_desc";
            ViewBag.CategorySort = sortOrder == "category_desc" ? "category_asc" : "category_desc";
            ViewBag.CustomerSort = sortOrder == "customer_desc" ? "customer_asc" : "customer_desc";

            Func<IQueryable<Order>, IOrderedQueryable<Order>> orderBy = null;
            switch (sortOrder)
            {
                case "product_desc":
                    orderBy = x => x.OrderByDescending(x => x.Product.Name);
                    break;
                case "product_asc":
                    orderBy = x => x.OrderBy(x => x.Product.Name);
                    break;
                case "category_desc":
                    orderBy = x => x.OrderByDescending(x => x.Product.Category.Name);
                    break;
                case "category_asc":
                    orderBy = x => x.OrderBy(x => x.Product.Category.Name);
                    break;
                case "customer_desc":
                    orderBy = x => x.OrderByDescending(x => x.Customer.Name);
                    break;
                case "customer_asc":
                    orderBy = x => x.OrderBy(x => x.Customer.Name);
                    break;
                default:
                    orderBy = x => x.OrderByDescending(x => x.Product.Name);
                    break;
            }

            var orders = await orderService.GetAll(keyword,filter, orderBy: orderBy, pageSize, pageIndex);

            return View(orders);
        }

        public async Task<IActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Order orderVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["InforCreate"] = "Create fail";
                return View(orderVm);
            }

            var isSuccess = await orderService.Create(orderVm);

            if (isSuccess)
            {
                TempData["InforCreate"] = "Create success";
                return RedirectToAction("Index", "Order");
            }
            return View(orderVm);
        }

        public async Task<IActionResult> Edit(int id)
        {
            var orderVm = await orderService.GetById(id);

            if (orderVm != null)
                return View(orderVm);

            return RedirectToAction("Index", "Order");
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Order orderVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["InforUpdate"] = "Update fail";
                return View(orderVm);
            }

            var isSuccess = await orderService.Update(orderVm);

            if (isSuccess)
            {
                TempData["InforUpdate"] = "Update success";
                return RedirectToAction("Index", "Order");
            }
            return View(orderVm);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var orderVm = await orderService.GetById(id);

            if (orderVm != null)
                return View(orderVm);

            return RedirectToAction("Index", "Order");
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id, IFormCollection collection)
        {
            if (!ModelState.IsValid)
                TempData["InforDelete"] = "Delete fail";

            var isSuccess = await orderService.Delete(id);

            if (isSuccess)
            {
                TempData["InforDelete"] = "Delete success";
                return RedirectToAction("Index", "Order");
            }

            return View();
        }
    }
}
