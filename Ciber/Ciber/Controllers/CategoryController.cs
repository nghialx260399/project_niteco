﻿using Ciber.Models.Entities;
using Ciber.Services.Categories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Ciber.Controllers
{
    [Authorize]
    public class CategoryController : Controller
    {
        private readonly ICategoryService categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            this.categoryService = categoryService;
        }
        public async Task<IActionResult> Index()
        {
            var categories = await categoryService.GetAll();

            return View(categories);
        }

        public async Task<IActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Category categoryVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["InforCreate"] = "Create fail";
                return View(categoryVm);
            }

            var isSuccess = await categoryService.Create(categoryVm);

            if (isSuccess)
            {
                TempData["InforCreate"] = "Create success";
                return RedirectToAction("Index", "Category");
            }
            return View(categoryVm);
        }

        public async Task<IActionResult> Edit(int id)
        {
            var categoryVm = await categoryService.GetById(id);

            if (categoryVm != null)
                return View(categoryVm);

            return RedirectToAction("Index", "Category");
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Category categoryVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["InforUpdate"] = "Update fail";
                return View(categoryVm);
            }

            var isSuccess = await categoryService.Update(categoryVm);

            if (isSuccess)
            {
                TempData["InforUpdate"] = "Update success";
                return RedirectToAction("Index", "Category");
            }
            return View(categoryVm);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var categoryVm = await categoryService.GetById(id);

            if (categoryVm != null)
                return View(categoryVm);

            return RedirectToAction("Index", "Category");
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id, IFormCollection collection)
        {
            if (!ModelState.IsValid)
                TempData["InforDelete"] = "Delete fail";

            var isSuccess = await categoryService.Delete(id);

            if (isSuccess)
            {
                TempData["InforDelete"] = "Delete success";
                return RedirectToAction("Index", "Category");
            }

            return View();
        }
    }
}
