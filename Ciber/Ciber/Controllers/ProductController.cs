﻿using Ciber.Models.Entities;
using Ciber.Services.Products;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Ciber.Controllers
{
    [Authorize]
    public class ProductController : Controller
    {
        private readonly IProductService productService;

        public ProductController(IProductService productService)
        {
            this.productService = productService;
        }
        public async Task<IActionResult> Index()
        {
            var products = await productService.GetAll();

            return View(products);
        }

        public async Task<IActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Product productVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["InforCreate"] = "Create fail";
                return View(productVm);
            }

            var isSuccess = await productService.Create(productVm);

            if (isSuccess)
            {
                TempData["InforCreate"] = "Create success";
                return RedirectToAction("Index", "Product");
            }
            return View(productVm);
        }

        public async Task<IActionResult> Edit(int id)
        {
            var productVm = await productService.GetById(id);

            if (productVm != null)
                return View(productVm);

            return RedirectToAction("Index", "Product");
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Product productVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["InforUpdate"] = "Update fail";
                return View(productVm);
            }

            var isSuccess = await productService.Update(productVm);

            if (isSuccess)
            {
                TempData["InforUpdate"] = "Update success";
                return RedirectToAction("Index", "Product");
            }
            return View(productVm);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var productVm = await productService.GetById(id);

            if (productVm != null)
                return View(productVm);

            return RedirectToAction("Index", "Product");
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id, IFormCollection collection)
        {
            if (!ModelState.IsValid)
                TempData["InforDelete"] = "Delete fail";

            var isSuccess = await productService.Delete(id);

            if (isSuccess)
            {
                TempData["InforDelete"] = "Delete success";
                return RedirectToAction("Index", "Product");
            }

            return View();
        }
    }
}
