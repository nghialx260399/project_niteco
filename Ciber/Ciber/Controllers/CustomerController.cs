﻿using Ciber.Models.Entities;
using Ciber.Services.Customers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Ciber.Controllers
{
    [Authorize]
    public class CustomerController : Controller
    {
        private readonly ICustomerService customerService;

        public CustomerController(ICustomerService customerService)
        {
            this.customerService = customerService;
        }
        public async Task<IActionResult> Index()
        {
            var categories = await customerService.GetAll();

            return View(categories);
        }

        public async Task<IActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Customer customerVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["InforCreate"] = "Create fail";
                return View(customerVm);
            }

            var isSuccess = await customerService.Create(customerVm);

            if (isSuccess)
            {
                TempData["InforCreate"] = "Create success";
                return RedirectToAction("Index", "Customer");
            }
            return View(customerVm);
        }

        public async Task<IActionResult> Edit(int id)
        {
            var categoryVm = await customerService.GetById(id);

            if (categoryVm != null)
                return View(categoryVm);

            return RedirectToAction("Index", "Customer");
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Customer customerVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["InforUpdate"] = "Update fail";
                return View(customerVm);
            }

            var isSuccess = await customerService.Update(customerVm);

            if (isSuccess)
            {
                TempData["InforUpdate"] = "Update success";
                return RedirectToAction("Index", "Customer");
            }
            return View(customerVm);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var categoryVm = await customerService.GetById(id);

            if (categoryVm != null)
                return View(categoryVm);

            return RedirectToAction("Index", "Customer");
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id, IFormCollection collection)
        {
            if (!ModelState.IsValid)
                TempData["InforDelete"] = "Delete fail";

            var isSuccess = await customerService.Delete(id);

            if (isSuccess)
            {
                TempData["InforDelete"] = "Delete success";
                return RedirectToAction("Index", "Customer");
            }

            return View();
        }
    }
}
