using Ciber.Infrastructure.IRepositories;
using Ciber.Models.Entities;
using Ciber.Services.Orders;
using Ciber.ViewModels.BaseEntityVms;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Moq;
using NUnit.Framework;

namespace Ciber.UnitTests
{
    [TestFixture]
    public class OrderTests
    {
        private Mock<IOrderRepository> _mockOrderRepository;
        private IOrderService _orderService;
        [SetUp]
        public void Setup(IOrderService _orderService)
        {
            _mockOrderRepository = new Mock<IOrderRepository>();
            _orderService = _orderService;
        }

        [Test]
        public void GetAll_GetAllOrders_ReturnAllOrders()
        {
            var orders = new List<Order>()
            {
                new Order(){Id=1, ProductId = 2, CustomerId = 1, Amount = 5},
                new Order(){Id=2, ProductId = 2, CustomerId = 1, Amount = 6},
            };

            var pageVm = new PagedVm<Order>(orders as IQueryable<Order>, 1, 2, 2);
            _mockOrderRepository.Setup(o => o.GetAllWithPaging()).Returns(pageVm);
            PagedVm<Order> result = _orderService.GetAll();

            Assert.That(result.Data.Count, Is.EqualTo(2));
        }
    }
}